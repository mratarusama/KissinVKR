using UnityEngine.UI;
using UnityEngine;
using Defective.JSON;
using TMPro;
using System;

public class JSONLoader : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _title, _discription, _mName;
    [SerializeField] private Image _mLogo, _objImg;
    [SerializeField] private Museum museumPrefab;
    [SerializeField] private Transform museumParent;
    [SerializeField] private Exhibit exhibitPrefab;
    [SerializeField] private Transform exhibitParent;
    [SerializeField] private Transform viewParent;
    [SerializeField] private GameObject objPanel;
    [SerializeField] private GameObject search;
    [SerializeField] private GameObject viewPanel;
    [SerializeField] private GameObject orgPanel;
    private string _id = "1yJ3Xbay9NSx6wE-JOgSEK0fF33yjRsfw";

        //    {
        //    "MUSEUM1": {
        //        "m_id": "",
        //        "name": "ВДНХ",
        //        "m_image":"",
        //        "exhibits": [
        //            {
        //                "id": "1AcN53O8Tmlc7jJFT9tfja-nX0Jd5OAff",
        //                "pref_name":"vpo-s",
        //                "image": "",
        //                "title": "КОСМИЧЕСКИЙ АППАРАТ",
        //                "discription": "А вот здесь вот начинается описание этого аппартат и идёт оно и идёт до тех пор, пока не нчнётся завтеный, всем нам так хорошо знакомый текст: Появился, значит, в Зоне Чёрный сталкер. К лагерю ночью повадился ходить и там сует руку в палатку и говорит: \"Водички попить!\" А если не дашь хлебнуть из фляжки или наружу полезешь — пришибет! А раз мужик один решил пошутить: вылез тихо из палатки, надел кожаную перчатку и полез к соседям в палатку. Полез, значит, и попрошайничает жалостно: \"Водички, водички попить…\" А тут из палатки навстречу высовывается рука и за горло его — цап! И сиплый голосок отзывается тихонько: \"А тебе моя водичка зачем нужна?!\""
        //            },
        //            {
        //                "id": "17RUDOSsmmFL5dv-xpHrpYMqQe8iEyQHz",
        //                "pref_name":"robot",
        //                "image": "",
        //                "title": "ДРУГОЙ КОСМИЧЕСКИЙ АППАРАТ",
        //                "discription": "А вот здесь вот начинается описание этого аппартат и идёт оно и идёт до тех пор, пока не нчнётся завтеный, всем нам так хорошо знакомый текст: Появился, значит, в Зоне Чёрный сталкер. К лагерю ночью повадился ходить и там сует руку в палатку и говорит: \"Водички попить!\" А если не дашь хлебнуть из фляжки или наружу полезешь — пришибет! А раз мужик один решил пошутить: вылез тихо из палатки, надел кожаную перчатку и полез к соседям в палатку. Полез, значит, и попрошайничает жалостно: \"Водички, водички попить…\" А тут из палатки навстречу высовывается рука и за горло его — цап! И сиплый голосок отзывается тихонько: \"А тебе моя водичка зачем нужна?!\""
        //            },
        //            {
        //                "id": "",
        //                "pref_name":"",
        //                "image": "",
        //                "title": "СОВСЕМ ДРУГОЙ КОСМИЧЕСКИЙ АППАРАТ",
        //                "discription": "А вот здесь вот начинается описание этого аппартат и идёт оно и идёт до тех пор, пока не нчнётся завтеный, всем нам так хорошо знакомый текст: Появился, значит, в Зоне Чёрный сталкер. К лагерю ночью повадился ходить и там сует руку в палатку и говорит: \"Водички попить!\" А если не дашь хлебнуть из фляжки или наружу полезешь — пришибет! А раз мужик один решил пошутить: вылез тихо из палатки, надел кожаную перчатку и полез к соседям в палатку. Полез, значит, и попрошайничает жалостно: \"Водички, водички попить…\" А тут из палатки навстречу высовывается рука и за горло его — цап! И сиплый голосок отзывается тихонько: \"А тебе моя водичка зачем нужна?!\""
        //            }
        //        ]
        //    }
        //    "MUSEUM2": {
        //        "m_id": "", .....
        //    }
        //}

    /*[Serializable]
    public struct Config
    {
        [Serializable]
        public struct Exhibit
        {
            [field: SerializeField] public string id, pref_name, image, title, discription;
        };

        [Serializable]
        public struct Museum
        {
            [field: SerializeField] public string m_id, name, m_image;
            [field: SerializeField] public Exhibit[] exhibits;
        };

        [field: SerializeField] public Museum Museum1;
    }*/

    void Start()
    {
        new UIController();
        StartCoroutine(Api.GetRequest(_id, (_error, _jsonString) =>
        {
            JSONObject obj = new JSONObject(_jsonString);
            


            Debug.Log(obj);
            var _museums = obj.GetField("museums");
            for (int i = 0; i < _museums.count; i++)
            {
                var dataMus = _museums[i];
                var mName = dataMus.GetField("name").ToString();
                //тут что-то с логотипом
                var Instance = Instantiate(museumPrefab, museumParent);
                Instance.name = "DarkBut [" + mName + ']';
                Instance.Init(mName, null, orgPanel, objPanel);
                var _exhibits = dataMus.GetField("exhibits");
                for (int j = 0; j < _exhibits.count; j++)
                {
                    var dataObj = _exhibits[j];
                    var title = dataObj.GetField("title").stringValue;
                    var discription = dataObj.GetField("discription").stringValue;
                    //тут что-то с картинкой
                    var id = dataObj.GetField("id").stringValue;
                    var prefabName = dataObj.GetField("pref_name").stringValue;
                    //var ViewInstance = Instantiate(viewPrefab, viewParent);
                    //ViewInstance.name = "ViewPanel [" + title + ']';
                    //viewPrefab.Init(title, null, discription);
                    var Instance1 = Instantiate(exhibitPrefab, exhibitParent);
                    Instance1.name = "BigBlackPanel [" + title + ']';
                    Instance1.Init(title, discription, null, id, prefabName, objPanel, search, viewPanel);

                }
            }
            
            
        }));
        
        
    }

}