using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Exhibit : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI discription;
    [SerializeField] private Image image;
    private ViewPanel viewPrefab;
    private GameObject objPanel;
    private GameObject search;
    private GameObject viewPanel;
    private Button button;
    private string id;
    private string prefabName;
    private string prefabDescription;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(()=>{
            ModelLoader._id = id;
            ModelLoader._prefabName = prefabName;
            objPanel.SetActive(false);
            search.SetActive(false);
            viewPanel.SetActive(true);
            GameObject.Find("View_Заголовок").GetComponent<TextMeshProUGUI>().text = title.text;
            GameObject.Find("View_Описание").GetComponent<TextMeshProUGUI>().text = prefabDescription;
        });
    }
    public void Init(string _title, string _discription, Sprite _sprite, string _id, string _prefabName, GameObject _objPanel, GameObject _search, GameObject _viewPanel)
    {
        prefabDescription = _discription;
        title.text = _title;
        discription.text = _discription.Length > 100 ? String.Format("{0}...", _discription.Substring(0, 100)) : _discription;
        //тут будет обработка картинки
        id = _id;
        prefabName = _prefabName;
        objPanel = _objPanel;
        search = _search;
        viewPanel = _viewPanel;
    }
}
