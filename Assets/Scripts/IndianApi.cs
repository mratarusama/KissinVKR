using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndianApi : MonoBehaviour
{
    string url = "https://drive.google.com/uc?export=download&id=1RGLNaU9zYpdB_X28iVoh_ZX_pHIUasEI";

    void Start()
    {
        WWW www = new WWW(url);
        StartCoroutine(WebReq(www));
    }

    IEnumerator WebReq(WWW www)
    {
            yield return www;
            while (www.isDone == false)
            {
                yield return null;
            }
        AssetBundle bundle = www.assetBundle;

        if (www.error == null) {
            GameObject obj = (GameObject)bundle.LoadAsset("vpo-s");
            Instantiate(obj);
        }

        else {
            Debug.Log(www.error);
        }
    }
}
