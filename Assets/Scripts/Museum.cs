using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Museum : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI mName;
    [SerializeField] private Image image;
    private GameObject orgPanel;
    private GameObject objPanel;
    private Button button;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => {
            objPanel.SetActive(true);
            orgPanel.SetActive(false);
        });
    }
    public void Init(string _mName, Sprite _sprite, GameObject _orgPanel, GameObject _objPanel)
    {
        mName.text = _mName;
        //��� ����� ��������� ��������
        objPanel = _objPanel;
        orgPanel = _orgPanel;
    }
}
